//
//  ViewController.swift
//  Exa_prac_1_eva_1_12550554
//
//  Created by TEMPORAL2 on 27/09/16.
//  Copyright © 2016 TEMPORAL2. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBOutlet weak var acreditado: UILabel!

    @IBOutlet weak var calificacion: UILabel!
    
    @IBOutlet weak var acreditar: UILabel!
    
    var ac = 0
    var cal = 0
    
    @IBAction func acreditar(sender: UISlider) {
        acreditar.text = "Puntos Para Acreditar: \(lroundf(sender.value))"
        ac = lroundf(sender.value)
    }

    @IBAction func calificacion(sender: UISlider) {
        calificacion.text = "Calificacion: \(lroundf(sender.value))"
        cal = lroundf(sender.value)
        if (cal >= ac) {
            acreditado.text = "Acreditado"
        }else {
            acreditado.text = "No Acreditado"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

